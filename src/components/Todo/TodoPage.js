/**
 * Created by shawn on 2015-11-24.
 */

import React, { PropTypes, Component } from 'react';
import styles from './TodoPage.css';
import withStyles from '../../decorators/withStyles';
import Link from '../Link';


@withStyles(styles)
class TodoPage extends Component {
  static contextTypes = {
    onSetTitle: PropTypes.func.isRequired, //onSetTitle is a required function type that must be set
  };

  render() {
    const title = 'To Do List'; //Setting title constant
    this.context.onSetTitle(title); //Setting the required title context

    return (
      <div className="TodoPage">
        <h1>{title}</h1>
        <List />
      </div>
    )

  }

}

export default TodoPage;
