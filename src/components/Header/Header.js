/*! React Starter Kit | MIT License | http://www.reactstarterkit.com/ */

import React, { Component } from 'react';
import styles from './Header.css';
import withStyles from '../../decorators/withStyles';
import Link from '../Link';
import Navigation from '../Navigation';

@withStyles(styles)
class Header extends Component {

  render() {
    return (
      <div className="Header">
        <div className="Header-container">
          <Navigation className="Header-nav" />
        </div>
        <div className="Header-banner">
          <h1 className="Header-bannerTitle">React TODO</h1>
          <p className="Header-bannerDesc">Learning to make TODO</p>
        </div>
      </div>
    );
  }

}

export default Header;
